<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function show()
    {
        return view('admin.sign-in');
    }

    public function login(Request $request)
    {
        if (Auth::attempt([
            'name' => $request->input('name'),
            'password' => $request->input('password')

        ])) {
            dd(Auth::user());
            return view('admin.index');
        }
        dd(Auth::user());
        return redirect()->back()->with('error', 'неверный логин или пароль');
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect()->intended();
    }
}
