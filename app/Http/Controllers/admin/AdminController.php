<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function view;

class AdminController extends Controller
{
    public function index(){
        $posts = DB::table('posts')->get()->sortKeysDesc();
        return view('blog.index', ['posts' => $posts]);
        return view('admin.index');
    }
}
