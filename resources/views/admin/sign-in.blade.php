@extends('layouts.baseindex')
@section('title', 'Новый пост')
@section('content')
    <div class="col-md-4"></div>
    <div class="text-center col-md-4">
        <form action="{{ route('log-in') }}" method="post" class="form-signin">
            @csrf
            <h1 class="h3 mb-3 font-weight-normal">Авторизация</h1>
            <label for="inputNae" class="sr-only">Введите имя</label>
            <input type="text" id="inputNae" class="form-control" placeholder="Name" required autofocus>
            <label for="inputPassword" class="sr-only">Введите пароль</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
        </form>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
@endsection
