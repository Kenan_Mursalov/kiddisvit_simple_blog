@extends('layouts.baseindex')
@section('title', 'Новый пост')
@section('content')
    <form action="{{ route('create-post') }}" method="post">
        @csrf
        <input type="text" name="title" id="">
        <input type="text" name="content" id="">
        <button type="submit">Создать пост</button>
    </form>

@endsection
