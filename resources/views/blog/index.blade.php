@extends('layouts.baseindex')
@section('title', 'blog')
@section('content')
@foreach ($posts as $post)
    <div class="col-md-4">
        <h2>{{ $post->title }}</h2>
        {{ $post->title }}
        <p>{{ $post->content }}</p>
        <a  class="btn btn-primary" href="{{ route('post_by_id', $post->id) }}">Прочитать</a>
    </div>
@endforeach
@endsection
