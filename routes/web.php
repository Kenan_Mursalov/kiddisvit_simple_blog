<?php

use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);
Route::get('post/{id}', [PostController::class, 'show'])->name('post_by_id');
Route::get('make-user', [UserController::class, 'createUser'])->name('make-user');

Route::prefix('admin')->group(function () {
    Route::get('/', [AdminController::class, 'index']);
    Route::get('/create-post',  [PostController::class, 'create']);
    Route::get('/edit-post',  [PostController::class, 'edit']);
    Route::post('/log-in', [LoginController::class, 'login'])->name('log-in');
//    Route::get('/log-in',  [PostController::class, 'index']);
    Route::post('/create-post', [PostController::class, 'store'])->name('create-post');
});
